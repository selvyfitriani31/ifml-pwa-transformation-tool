from custom_xmi_parser.xmiparser_2 import parse as uml_parse
from main.utils.ast.framework.angular.models import ModelFromUMLClass


def convert_json(json_path):
    xmi_stucture_json = uml_parse(json_path)
    model_from_class = []
    for _, xmi_class in xmi_stucture_json.get_classes().items():
        model_from_class.append(convert_json_class(xmi_class))
    return model_from_class


def convert_json_class(xmi_class):
    element_name = xmi_class.get_model_name()
    model_from_class = ModelFromUMLClass(element_name)
    for _, attribute in xmi_class.get_properties().items():
        model_from_class.add_owned_attribute_to_class(attribute.get_model_name(), attribute.get_type())
    return model_from_class



