from pathlib import Path
import json
import os
import re


def create_min_validator():
    # create and jump to a directory
    os.mkdir('custom-min-validator')
    os.chdir('custom-min-validator')

    # create ts file
    min_file = open('custom-min-validator.directive.ts', 'w+')

    min_text = """import { Directive, Input } from '@angular/core';
        import { NG_VALIDATORS, Validator, FormControl } from '@angular/forms';

        @Directive({
            selector: '[customMin][formControlName],[customMin][formControl],[customMin][ngModel]',
            providers: [{ provide: NG_VALIDATORS,
                useExisting: CustomMinDirective, multi: true }]
        })
        export class CustomMinDirective implements Validator {
            @Input()
            customMin: number;

            validate(c: FormControl): { [key: string]: any } {
                let v = c.value;
                return (v < this.customMin) ? { "customMin": true } : null;
            }
        }
        """
    min_file.write(min_text)

    os.chdir('..')


def create_max_validator():
    # create and jump to a directory
    os.mkdir('custom-max-validator')
    os.chdir('custom-max-validator')

    # create ts file
    max_file = open('custom-max-validator.directive.ts', 'w+')

    max_text = """import { Directive, Input } from '@angular/core';
        import { NG_VALIDATORS, Validator, FormControl } from '@angular/forms';

        @Directive({
            selector: '[customMax][formControlName],[customMax][formControl],[customMax][ngModel]',
            providers: [{ provide: NG_VALIDATORS,
                useExisting: CustomMaxDirective, multi: true }]
        })
        export class CustomMaxDirective implements Validator {
            @Input()
            customMax: number;

            validate(c: FormControl): { [key: string]: any } {
                let v = c.value;
                return (v > this.customMax) ? { "customMax": true } : null;
            }
        }
        """
    max_file.write(max_text)

    os.chdir('..')


def add_min_max_to_module():
    import_max = """import {
        CustomMinDirective
    } from './custom-min-validator/custom-min-validator.directive';\n"""
    import_min = """import {
        CustomMaxDirective
    } from './custom-max-validator/custom-max-validator.directive';\n"""
    max_directive = "\t\tCustomMaxDirective,\n"
    min_directive = "\t\tCustomMinDirective,\n"
    add_to_imported = import_max + import_min
    add_to_declaration = max_directive + min_directive
    empty_space = 0
    lines = ""

    with open('app.module.ts', 'r') as module_file:
        lines = module_file.readlines()
        isExist = False

        for index, line in enumerate(lines):
            if (not isExist):
                if line.startswith('@NgModule'):
                    # add to imported module
                    lines[index-1] = re.sub(
                        '\n', add_to_imported, lines[index-1])

                    # declare 2 empty line for directive declaration
                    first_component = lines[index+2]
                    lines[index+2] = first_component + '\n'
                    empty_space = index+8

                    isExist = True
            else:
                break

    out = open('app.module.ts', 'w')
    out.writelines(lines)
    out.close()

    print(empty_space)
    with open('app.module.ts', 'r') as module_file:
        lines = module_file.readlines()
        lines[empty_space] = add_to_declaration

    out = open('app.module.ts', 'w')
    out.writelines(lines)
    out.close()


def build_min_max_file(product_name):
    
    os.chdir("result/{}/src/app".format(product_name))

    create_min_validator()
    create_max_validator()
    add_min_max_to_module()
