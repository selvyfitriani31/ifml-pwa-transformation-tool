from main.utils.ast.base import Node
from main.utils.jinja.angular import base_file_writer

class OauthRedirectTypescriptClass(Node):

    def __init__(self):
        super().__init__()
        self.selector_name = 'oauth-redirect'
        self.class_name = 'OauthRedirect'

    def render(self):
        return base_file_writer('src/app/oauth-redirect/oauth-redirect.component.ts.template')

    def get_class_name(self):
        return self.class_name

class OauthRedirectHTML(Node):

    def __init__(self):
        super().__init__()

    def render(self):
        return base_file_writer('src/app/oauth-redirect/oauth-redirect.component.html.template')