import sys
sys.path.append('/Users/affandhia/Documents/Affan/skripsi/ifml-angular-pwa')
from main.core.angular.main import generate_project
import logging
from ifml_parser.ifmlxmiparser import parse as ifml_parse
from custom_xmi_parser.xmiparser_2 import parse as uml_parse
import coloredlogs

ast_uml, uml_table = uml_parse('RSE_financial_report.uml')
ast_ifml, ifml_table = ifml_parse(xmiFileName='summary.core', umlSymbolTable=uml_table)